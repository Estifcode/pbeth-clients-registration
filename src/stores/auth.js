import {defineStore} from 'pinia';
import axios from '../axios';
// export const useAuthStore = defineStore({ id: 'auth', 
export const useAuthStore = defineStore('auth', {
    state: () =>({
        authUser: null,
        firstName: "",
        email: "",
        role: ""
    }),
    getters: {
        user: (state) => state.authUser    },
    actions:{

        async getUser(){
            this.getToken();
            const data = await axios.get('/auth/signup');

            this.authUser = data.data; 
        },
        async login(email, password) {
            const result = await axios.post('/auth/login', {
                email, password
            })
                .then(res => {
                    const { email, name, role } = res.data 

                    this.firstName = name
                    this.email = email
                    this.role = role
                })
                .catch(error => {
                    throw new Error(error.message)
                })
        }

    }

});

export default useAuthStore;